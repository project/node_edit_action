# Node Edit Action

This module provides power users the ability to edit multiple pieces of content
at the same time on the same page. This cuts down the time content editors will
need to update multiple content.

## Requirements

* Drupal 9/10
* PHP 7.4+

## Installation

This module should be installed with Composer.

```bash
$ composer require drupal/node_edit_action
```

## Configuration

The module has no configuration, but users will be required to have the
`administer nodes` permission to use this action.

## Troubleshooting

There may be some issues with other modules who are not correctly leveraging the
Field API. Please open an issue on the project for the maintainers to
investigate.

## Maintainers

Development of this module is sponsored by [Mythic Digital][].

* Nathan Dentzau (nathan@mythicdigital.io)

[Mythic Digital]: https://mythicdigital.io
