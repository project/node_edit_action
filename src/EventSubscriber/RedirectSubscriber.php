<?php

declare(strict_types = 1);

namespace Drupal\node_edit_action\EventSubscriber;

use Drupal\Core\Url;
use Drupal\node_edit_action\Exception\RedirectToFormException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides a redirect subscriber from the bulk action plugin.
 */
class RedirectSubscriber implements EventSubscriberInterface {

  /**
   * Catch and redirect the RedirectToFormException.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The exception event.
   */
  public function onKernelException(ExceptionEvent $event): void {
    $exception = $event->getThrowable();

    if (!$exception instanceof RedirectToFormException) {
      return;
    }

    $url = Url::fromRoute('node_edit_action.form')->toString();
    $response = new RedirectResponse($url);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::EXCEPTION => [['onKernelException', 1000]],
    ];
  }

}
