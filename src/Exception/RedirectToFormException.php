<?php

declare(strict_types = 1);

namespace Drupal\node_edit_action\Exception;

/**
 * Provides a custom exception to handle redirects to the edit form.
 *
 * This pattern is adopted from the AJAX API where an early exit of execution
 * of the code needs to occur and a redirect needs to be made. The Actions API
 * does not allow you to return a response in an action plugin, so this is the
 * _best_ approach at the time of creating this module to handle a redirect in
 * an action plugin.
 */
class RedirectToFormException extends \Exception {
}
