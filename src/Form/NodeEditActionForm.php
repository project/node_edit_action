<?php

declare(strict_types = 1);

namespace Drupal\node_edit_action\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to edit multiple nodes at the same time.
 */
class NodeEditActionForm extends FormBase {

  /**
   * The private temporary store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $store;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A private cache of loaded entities.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  private $entities = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('tempstore.private')->get('node_edit_action'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructor for NodeEditActionForm.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStore $store
   *   The private temporary store.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    PrivateTempStore $store,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->store = $store;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'node_edit_action';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['top_message'] = [
      '#markup' => <<<MARKUP
        To edit an individual piece of content, click on the content title and
        the form will expand. It's recommended to collapse each content form
        after editing is complete to reduce the amount of vertical scrolling.
      MARKUP,
    ];

    foreach ($this->getEntities() as $entity) {
      /** @var \Drupal\node\NodeInterface $entity */
      $form[$entity->uuid()] = [
        '#type' => 'details',
        '#title' => $entity->getTitle(),
        '#open' => FALSE,
        'form' => [
          '#type' => 'container',
          '#process' => [[$this, 'processEntityForm']],
          '#entity' => $entity,
        ],
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ],
      'cancel' => [
        '#type' => 'submit',
        '#value' => $this->t('Cancel'),
        '#submit' => [[$this, 'cancelForm']],
      ],
    ];

    return $form;
  }

  /**
   * Render the entity form.
   *
   * @param array $element
   *   The form element the entity form is in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The entity form.
   */
  public function processEntityForm(array $element, FormStateInterface $form_state): array {
    $entity = $element['#entity'];
    $element['#parents'] = [$entity->uuid()];

    EntityFormDisplay::collectRenderDisplay($entity, 'edit')
      ->buildForm($entity, $element, $form_state);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    foreach ($this->getEntities() as $entity) {
      $entity_form = $form[$entity->uuid()]['form'];
      $entity = $entity_form['#entity'];

      $form_display = EntityFormDisplay::collectRenderDisplay(
        $entity,
        'edit'
      );
      $complete_form_state = $form_state instanceof SubformStateInterface
        ? $form_state->getCompleteFormState()
        : $form_state;
      $form_display->extractFormValues(
        $entity,
        $entity_form,
        $complete_form_state
      );
      $form_display->validateFormValues(
        $entity,
        $entity_form,
        $complete_form_state
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    foreach ($this->getEntities() as $entity) {
      $entity_form = $form[$entity->uuid()]['form'];
      $entity = $entity_form['#entity'];

      $form_display = EntityFormDisplay::collectRenderDisplay(
        $entity,
        'edit'
      );
      $complete_form_state = $form_state instanceof SubformStateInterface
        ? $form_state->getCompleteFormState()
        : $form_state;
      $form_display->extractFormValues(
        $entity,
        $entity_form,
        $complete_form_state
      );

      $entity->save();
    }

    $this->messenger()->addStatus(
      $this->t(
        '@count nodes have been updated',
        ['@count' => count($this->getEntities())]
      )
    );

    $form_state->setRedirect('system.admin_content');

    $this->store->delete('entities');
  }

  /**
   * Cancel the edit action and remove the entities being edited in the store.
   *
   * @param array $form
   *   The form build.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancelForm(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('system.admin_content');
    $this->store->delete('entities');
  }

  /**
   * Load the entities to be edited from the user's temporary store.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities to be edited.
   */
  protected function getEntities(): array {
    if (empty($this->entities)) {
      $entities = $this->store->get('entities');
      if (empty($entities)) {
        throw new NotFoundHttpException();
      }

      $entities = unserialize($entities, ['allowed_classes' => FALSE]);
      $this->entities = $this->entityTypeManager
        ->getStorage('node')
        ->loadMultiple($entities);
    }

    return $this->entities;
  }

}
