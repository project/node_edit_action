<?php

declare(strict_types = 1);

namespace Drupal\node_edit_action\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\node_edit_action\Exception\RedirectToFormException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a bulk action to edit multiple content.
 *
 * @Action(
 *   id = "node_edit_action",
 *   label = @Translation("Edit content"),
 *   type = "node"
 * )
 */
class NodeEditAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The private temporary store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.private')->get('node_edit_action')
    );
  }

  /**
   * Constructor for NodeEditAction.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\TempStore\PrivateTempStore $store
   *   The private temporary store.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    PrivateTempStore $store
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->store = $store;
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities): void {
    $this->store->set(
      'entities',
      serialize(array_map(fn($node) => $node->id(), $entities))
    );

    // The Action API does not allow us to return a response, and thus a method
    // to safely exit from executing the code and redirecting is highly desired.
    // The approach to throw a specific exception and catch the exception to
    // redirect is a pattern used by the AJAX API.
    throw new RedirectToFormException();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    // Stub this method. We don't need it.
  }

  /**
   * {@inheritdoc}
   */
  public function access(
    $object,
    ?AccountInterface $account = NULL,
    $return_as_object = FALSE
  ) {
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
